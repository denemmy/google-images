#!/usr/bin/python2
# coding: utf8

import json
import os
import time
import requests

IMAGES_PER_PAGE = 100
SLEEP_AFTER_FAIL = 60  # in seconds
SLEEP_PER_REQUEST = 2

def save_images(img_list, start, limit, out_dir):
    image_index = start
    images_saved = 0
    for img_ref in img_list:
        url = img_ref
        img_name = 'img_{0:05d}.jpg'.format(image_index)
        img_path = os.path.join(out_dir, img_name)
        try:
            image_r = requests.get(url, timeout=4, stream=True)
            if image_r.ok:
                with open(img_path, 'wb') as handle:
                    for block in image_r.iter_content(1024):
                        handle.write(block)
        except Exception as e:
            print('could not download {}'.format(url))
            continue

        url_img_name = os.path.basename(url)
        print(u'image [{0}] {1} successfully saved.'.format(image_index, url_img_name))
        image_index += 1
        images_saved += 1
        # control maximum number of images
        if image_index >= limit:
            break
    return images_saved


def google_images(g, query, max_results, is_face, is_grayscale, out_dir):

    # base_url = 'https://www.google.ru/search?q=' + query + '&source=lnms&tbm=isch&tbs=isz:m'
    base_url = 'https://www.google.ru/search?q=' + query + '&source=lnms&tbm=isch'

    tbs_flags = []
    if is_face:
        tbs_flags.append('itp:face')
    if is_grayscale:
        tbs_flags.append('ic:gray')
    if len(tbs_flags) > 0:
        base_url += '&tbs={}'.format(','.join(tbs_flags))

    if max_results is not None:
        limit = max_results
    else:
        limit = IMAGES_PER_PAGE

    saved_number = 0
    images_to_load = limit - saved_number

    # do until required number of images are loaded
    page = 0
    while images_to_load > 0:
        if page > 0:
            page_str = str(page)
            start_offset = IMAGES_PER_PAGE * page
            start_offset_str = str(start_offset)
            page_url = base_url + '&ijn=' + page_str + '&start=' + start_offset_str + '&newwindow=1'
        else:
            page_url = base_url

        max_attempts = 5
        attempt = 1
        success = False
        while attempt <= max_attempts:
            try:
                print('sleep..')
                time.sleep(SLEEP_PER_REQUEST)
                print('awake.')
                g.go(page_url)
                img_data_selector = '//div[contains(@class, "rg_bx") and contains(@class, "ivg-i")]/div[contains(@class, "rg_meta")]//text()'
                img_data_list = g.doc.select(img_data_selector).node_list()

                if len(img_data_list) == 0:
                    print('stop loading because response is empty')
                    images_to_load = 0
                    break

                imgs_urls = map(lambda x: json.loads(x)['ou'], img_data_list)

                res = save_images(imgs_urls, saved_number, limit, out_dir)
                saved_number += res
                images_to_load = limit - saved_number
                page += 1
                success = True
                break
            except Exception as e:
                str_srr = 'error while searching in google.images: {}'.format(e)
                print('{0}, query = "{1}", attempt = [{2}/{3}]'.format(str_srr, query, attempt, max_attempts))
                print('sleep for some time...')
                time.sleep(SLEEP_AFTER_FAIL)
                print('awake.')
                attempt += 1
        if not success:
            images_to_load = 0

